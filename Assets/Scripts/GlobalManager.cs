﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class GlobalManager : MonoBehaviour{

    public static GlobalManager global;

	public GameObject InputFieldCanvas;
	public string usuario;
	public int score;

	public int numeroDeUsuarios;

	public List<int> scores;
	public List<string> users;

    public int col;
    public int row;

    public List<int> wut = new List<int> ();
	void Start () {
        if (global == null) {
            DontDestroyOnLoad (gameObject);
            global = this;
        } else if (global != this) {
            Destroy (gameObject);
        }

		if (PlayerPrefs.GetInt ("numeroDeUsuarios") == 0) {
			numeroDeUsuarios = 0;
		} else {
			numeroDeUsuarios = PlayerPrefs.GetInt ("numeroDeUsuarios");
		}

        //Agrega las listas cuando se inicia el juego
        for (int i = 0; i < numeroDeUsuarios; i++) {
            scores.Add (PlayerPrefs.GetInt ("score" + i));
            users.Add (PlayerPrefs.GetString ("usuario" + i));
        } 
	}

    public void SetScore () {
        numeroDeUsuarios++;
        PlayerPrefs.SetInt ("numeroDeUsuarios", numeroDeUsuarios);

        PlayerPrefs.SetInt ("score" + scores.Count, score);
        scores.Add (score);
        //PlayerPrefs.SetString ("usuario" + users.Count, usuario);
        //users.Add (usuario);

        scores.Sort ();//Ordena de menor a mayor
        scores.Reverse ();//Invierte la lista

        score = 0;
    }

	public int GetScore(int N){
        return PlayerPrefs.GetInt ("score" + N);
	}

	public void DelPlayerPrefs(){
        scores.Clear ();
        //users.Clear ();
        numeroDeUsuarios = 0;
		PlayerPrefs.DeleteAll ();
	}
}
