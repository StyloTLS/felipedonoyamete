﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneButtonManager : MonoBehaviour {

    public void GoToScene (int id) {
        SceneManager.LoadScene (id);
    }

    public void ActivateGameObject (GameObject obj) {
        obj.SetActive (true);
    }

    public void DeActivateGameObject (GameObject obj) {
        obj.SetActive (false);
    }

    public void SetTimeScale (float xTime) {
        Time.timeScale = xTime;
    }

    public void SubmitScore () {
        GlobalManager.global.SetScore ();
    }

    public void ResetScores () {
        GlobalManager.global.DelPlayerPrefs ();
        GameObject.Find ("ScoresManager").GetComponent<ScoreUI> ().UpdateScoreBoard ();
    }

    //UN POCO DE HARDCODEO NO LE HACE MAL A NADIE :3
    public void SetHighAndWide () {
        GameObject temp = GameObject.Find ("HighInput");
        GlobalManager.global.col = int.Parse (temp.GetComponent<InputField> ().text);

        temp = GameObject.Find ("WideInput");
        GlobalManager.global.row = int.Parse (temp.GetComponent<InputField> ().text);
    }
}
