﻿using UnityEngine;
using System.Collections;

public class InteractionManager : MonoBehaviour {

    public GameObject playerGame;
    public PlayerHolster player;
    public UIShowManager uiShow;

	// Use this for initialization
	void Start () {
        playerGame = GameObject.FindGameObjectWithTag ("Player");
        player = playerGame.GetComponent<PlayerHolster> ();
        uiShow = gameObject.GetComponent<UIShowManager> ();
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Judge (string tag, GameObject obj) {
        switch (tag) {
            case "Enemy":
                player.Damage ();
                Destroy (obj);
                uiShow.UpdateUI ();
                break;
            case "Heal":
                player.Heal ();
                Destroy (obj);
                uiShow.UpdateUI ();
                break;
            case "Coin":
                player.PickUpCoin ();
                Destroy (obj);
                uiShow.UpdateUI ();
                break;
            case "BadCoin":
                player.PickUpBadCoin();
                Destroy(obj);
                uiShow.UpdateUI();
                break;
            case "Teleport":
                GameManager.self.GenerateField (playerGame.transform.position.x, playerGame.transform.position.y);
                Destroy (obj);
                break;
            default:
                Debug.LogWarning (tag + " its not a registred tag");
                break;
        }
    }
}
