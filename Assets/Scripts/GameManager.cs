﻿using UnityEngine;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {

    //Atajos
    public GameObject touchCtrl;
    TouchControls tCtrl;
    public static GameManager self; // referencia estatica a la misma clase

    public Transform player;    //Referencia al jugador
    public Transform room;  //Padre al cual los tiles que conforman el nivel se asociaran

    public int columnas, filas; // Filas y columnas para la generacion del nivel

    //Prefabs
    public GameObject floorPrefab;
    public GameObject healPrefab;
    public GameObject slimePrefab;
    public GameObject coinPrefab;
    public GameObject badCoinPrefab;
    public GameObject teleportPrefab;

    public float offset; //distancia entre tiles

    public List<Vector3> positions;//lista donde se guardan las posiciones

    void Start () {
        //Definicion de variables
        if (GlobalManager.global.col < 5) {
            columnas = 5;
        } else {
            columnas = GlobalManager.global.col;
        }
        if (GlobalManager.global.row < 5) {
            filas = 5;
        } else {
            filas = GlobalManager.global.row;
        }

        //Inicializacion de atajos
        tCtrl = touchCtrl.GetComponent<TouchControls> ();
        self = this;
        //Creacion de Escena
        GenerateField (player.position.x, player.position.y);
    }

    public void GenerateField(float posX, float posY)
    {
        //Variables locales
		float Xoffset = 0;
		float Yoffset = 0;
		Vector3 pos;
        GameObject temp;

        #region Limpieza del padre Room y Lista de Posiciones
        for (int i = 0; i < room.childCount; i++) {
            Destroy (room.GetChild (i).gameObject);
        }

        positions.Clear ();
        #endregion

        #region Configuracion de limites para moverse negativos
        tCtrl.xLimitNegative = posX;
        tCtrl.yLimitNegative = posY;
        #endregion

        #region Zona de Generacion de Terreno
        for (float col = 0; col < columnas; col++)
		{
			for (float row = 0; row < filas; row++)
			{

				pos = new Vector3(posX + Xoffset, posY + Yoffset, transform.position.z);
                positions.Add (pos);
                temp =(GameObject) Instantiate (floorPrefab, pos, Quaternion.identity);
                temp.transform.SetParent (room);
                Yoffset += offset;

                #region Configuracion de limites para moverse positivos
                if (row == (filas - 1)) {
                    tCtrl.yLimitPositive = pos.y;
                }
                if(col == (columnas-1)) {
                    tCtrl.xLimitPositive = pos.x;
                }
                #endregion
            }
            Yoffset = 0;
			Xoffset += offset;
		}
        #endregion

        #region Generacion de Entidades
        
        for (int i = 1; i < positions.Count; i++) {
            if (i == positions.Count - 1) {
                //creacion de teleport
                temp = (GameObject)Instantiate (teleportPrefab, positions[i], Quaternion.identity);
                temp.transform.SetParent (room);
                break;
            }
            float n = Random.Range (1, 100);
            if (n <= 5) {
                temp = (GameObject) Instantiate (healPrefab, positions[i], Quaternion.identity);
                temp.transform.SetParent (room);
            } else if (n > 5 && n <= 15) {
                temp = (GameObject) Instantiate (badCoinPrefab, positions[i], Quaternion.identity);
                temp.transform.SetParent (room);
            } else if (n > 15 && n <= 35) {
                temp = (GameObject) Instantiate (coinPrefab, positions[i], Quaternion.identity);
                temp.transform.SetParent (room);
            } else if (n > 35 && n <= 60) {
                temp = (GameObject)Instantiate(slimePrefab, positions[i], Quaternion.identity);
                temp.transform.SetParent(room);
            } else if (n > 60 && n <= 100) {
                Debug.Log ("nope");
            }
        }
        #endregion
    }
}
