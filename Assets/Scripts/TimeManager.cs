﻿using UnityEngine;
using System.Collections;

public class TimeManager : MonoBehaviour {

    GameManager gM;
    public int a, b;
    public float timer;

	void Start () {
        gM = gameObject.GetComponent<GameManager>();
        a = gM.filas;
        b = gM.columnas;

        timer = (Mathf.Sqrt(Mathf.Pow(a, 2) + Mathf.Pow(b, 2))) / 1.5f;
	}
	
	// Update is called once per frame
	void Update () {
	    
	}
}


/*
   2v/(w^2 + h^2) 
    
     
 */