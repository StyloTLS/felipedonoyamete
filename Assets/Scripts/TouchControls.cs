﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;

public class TouchControls : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler
{

    public string direction;
    public int deltaMax;
    public float yLimitPositive;
    public float yLimitNegative;
    public float xLimitPositive;
    public float xLimitNegative;

    public GameObject player;

    public void OnBeginDrag(PointerEventData eventData)
    {
        //throw new NotImplementedException();
    }


    public void OnDrag(PointerEventData eventData)
    {
        //Debug.Log(eventData);

        if (Mathf.Abs(eventData.delta.x) > Mathf.Abs(eventData.delta.y))
        {
            Debug.Log("x:" + eventData.delta.x);
            if (eventData.delta.x >= deltaMax)
            {
                direction = "Right";
            } else if(eventData.delta.x <= -deltaMax)
            {
                direction = "Left";
            }
        }
        else
        {
            Debug.Log("y:" + eventData.delta.y);
            if (eventData.delta.y >= deltaMax)
            {
                direction = "Up";
            }
            else if(eventData.delta.y <= -deltaMax)
            {
                direction = "Down";
            }
        }
            
    }


    public void OnEndDrag(PointerEventData eventData)
    {
        //throw new NotImplementedException();
        switch (direction)
        {
            case "Right":
                if (player.transform.position.x < xLimitPositive)
                    player.transform.Translate(1,0,0);
                break;
            case "Left":
                if(player.transform.position.x > xLimitNegative)
                    player.transform.Translate(-1, 0, 0);
                break;
            case "Up":
                if (player.transform.position.y < yLimitPositive)
                    player.transform.Translate(0, 1, 0);
                break;
            case "Down":
                if (player.transform.position.y > yLimitNegative)
                    player.transform.Translate(0, -1, 0);
                break;
            default:
                break;
        }

        direction = "";
    }

}