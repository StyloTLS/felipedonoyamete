﻿using UnityEngine;
using UnityEngine.UI;

public class UIShowManager : MonoBehaviour {

    public Text hp;
    public Text score;
    public Text deadScore;
    public PlayerHolster player;

    public GameObject dead;

    void Start () {
        player = GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerHolster> ();
        UpdateUI ();
	}
	
	// Update is called once per frame
	void Update () {
	    
	}

    public void UpdateUI () {
        hp.text = "HP: " + player.health;
        score.text = "SCORE: " + GlobalManager.global.score;
        deadScore.text = "SCORE: " + GlobalManager.global.score;
    }

    public void KillPlayer () {
        dead.SetActive (true);
    }
}
