﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class ScoreUI : MonoBehaviour {

    public List<GameObject> places;

    void Start () {
        UpdateScoreBoard ();
	}

    public void UpdateScoreBoard () {
        GlobalManager.global.scores.Sort ();//Ordena de menor a mayor
        GlobalManager.global.scores.Reverse ();//Invierte la lista

        for (int i = 0; i < GlobalManager.global.scores.Count; i++) {
            places[i].GetComponent<Text> ().text = " " + (i + 1) + " ---> " + (GlobalManager.global.scores[i].ToString ());
        }
    }
}
