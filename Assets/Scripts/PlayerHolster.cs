﻿using UnityEngine;
using System.Collections;

public class PlayerHolster : MonoBehaviour {

    public GameManager itManager;
    InteractionManager it;

    public int maxHealth;
    public int health;

	void Start () {
        it = itManager.GetComponent<InteractionManager> ();
	}
	
    void OnTriggerEnter2D (Collider2D other) {
        it.Judge (other.tag,other.gameObject);
    }

    public void Damage () {
        health--;
        if (health <= 0) {
            it.uiShow.KillPlayer ();
        }
    }

    public void Heal () {
        if (health < maxHealth) {
            health++;
        }
    }

    public void PickUpCoin () {
        int x = Random.Range(1, 3);
        GlobalManager.global.score += 10*x;
    }
    public void PickUpBadCoin()
    {
        int x = Random.Range(1, 3);
        GlobalManager.global.score -= 10 * x;
    }
}
